---
Nom du Projet :  Ruche intelligente
---

# SmartBeeHive 

[![pipeline status](https://gitlab.com/ulrichkt2/smartbeehive/badges/main/pipeline.svg)](https://gitlab.com/ulrichkt2/smartbeehive/-/commits/main)

[![coverage report](https://gitlab.com/ulrichkt2/smartbeehive/badges/main/coverage.svg)](https://gitlab.com/ulrichkt2/smartbeehive/-/commits/main)




## Description
Ce projet est un prototype de ruche connectée pour la surveillance à distance des ruches urbaines

Ici le [cahier des charges du projet](./documents/Cahier_des_charges.pdf/)

## Lien des maquettes:

-  La maquette de l'application mobile se trouve [ici sur figma](https://www.figma.com/proto/wtqD3rAXFvEuO8Go6uDB7k/Stock-Management-(Community)?type=design&node-id=110-195&t=YWrjqm04YF0Wy0e7-1&scaling=scale-down&page-id=0%3A1&starting-point-node-id=4%3A10&mode=design) conçue par [Papa Madiodio DIENG](https://gitlab.com/papyDioDio)
-  La maquette de l'application Web se trouve [ici sur figma](https://www.figma.com/proto/nqPtjTzMxzyeOE1QpADcKk/Ruche-connect%C3%A9e?type=design&t=dIB23tM92vO8ogZ8-1&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=2%3A39&show-proto-sidebar=1&node-id=2-39&mode=design) conçue par [Ulrich KEMKA TAKENGNY](https://gitlab.com/ulrichkt2)

## Techno:

| Catégorie                      | Technologie        | Responsable                                          |
|--------------------------------|--------------------|------------------------------------------------------|
| Électronique                   | ESP32 + capteur    | Tout le monde                                        |
| Base de données                | Firebase           | Tout le monde                                        |
| [Backend](./backend/)          | Spring Boot        | [Khadim DAFFE](https://gitlab.com/czeed)             |
| [Application Web](./web/)      | React Js           | [Ulrich KEMKA TAKENGNY](https://gitlab.com/ulrichkt2)|
| [Application Mobile](./mobile/)| Flutter            | [Papa Madiodio DIENG](https://gitlab.com/papyDioDio) |

