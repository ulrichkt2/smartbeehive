---
Nom du Projet :  Ruche intelligente
---

# SmartBeeHive - Mobile


## Description
Cette partie contient l'application mobile du projet. Cette application est développée avec le framework [Flutter](https://flutter.dev/)


## Lien de la maquette:

-  La maquette de l'application mobile se trouve [ici sur figma](https://www.figma.com/proto/wtqD3rAXFvEuO8Go6uDB7k/Stock-Management-(Community)?type=design&node-id=110-195&t=YWrjqm04YF0Wy0e7-1&scaling=scale-down&page-id=0%3A1&starting-point-node-id=4%3A10&mode=design) conçue par [Papa Madiodio DIENG](https://gitlab.com/papyDioDio)
