package com.api.SmartBeehive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartBeehiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartBeehiveApplication.class, args);
	}

}
